<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use App\Http\Response\ApiResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response as ResponseFacade;

class ProductController extends ApiController
{

    private $allProductsAction;
    private $mostPopularProductAction;
    private $getCheapestProductsAction;

    public function __construct(
        GetCheapestProductsAction $getCheapestProductsAction,
        GetAllProductsAction $allProductsAction,
        GetMostPopularProductAction $mostPopularProductAction
    )
    {
        $this->allProductsAction = $allProductsAction;
        $this->mostPopularProductAction = $mostPopularProductAction;
        $this->getCheapestProductsAction = $getCheapestProductsAction;
    }

    public function getProductsCollection(): ApiResponse
    {

        return $this->createSuccessResponse(
            ProductArrayPresenter::presentCollection($this->allProductsAction->execute()->getProducts())
        );
    }

    public function getMostPopularProduct(): ApiResponse
    {
        return $this->createSuccessResponse(
            ProductArrayPresenter::present($this->mostPopularProductAction->execute()->getProduct())
        );
    }

    public function getCheapestProducts(): Response
    {
        $products = ProductArrayPresenter::presentCollection(
            $this->getCheapestProductsAction->execute()->getProducts()
        );
        return ResponseFacade::view('cheap_products', [
            'products' => $products
        ]);
    }
}
