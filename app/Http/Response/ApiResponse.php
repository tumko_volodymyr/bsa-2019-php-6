<?php

declare(strict_types = 1);

namespace App\Http\Response;

use App\Exceptions\ErrorCode;
use Illuminate\Http\JsonResponse;

final class ApiResponse extends JsonResponse
{
    private const NO_CONTENT_STATUS = 204;

    public static function success(array $data = []): self
    {
        return new static($data);
    }

    public static function empty(): self
    {
        return new static(null, self::NO_CONTENT_STATUS);
    }
}
