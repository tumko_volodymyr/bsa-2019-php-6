<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Illuminate\Support\Arr;

class ProductRepository implements ProductRepositoryInterface
{

    private $products = [];

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function findAll(): array
    {
        return $this->products;
    }

    public function getCheapestProducts(): array
    {
        $sortedByPrice = Arr::sort($this->products, function($product){
            return $product->getPrice();
        });
        return array_slice($sortedByPrice, 0, 3);
    }

    public function getMostPopularProduct(): ?Product
    {
        $first = $this->products[0]??null;
        return array_reduce($this->products, function ($carry, $item){
            return $carry->getRating()>$item->getRating()?$carry:$item;
        }, $first );
    }
}
