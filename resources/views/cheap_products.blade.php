<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="jumbotron">
    <h1>Cheap products</h1>
</div>
    <div class="container">
        <div class="row">
            @forelse ($products as $product)
                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <div class="card-header">
                            {{$product['name']}}
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Id: {{$product['id']}}</li>
                            <li class="list-group-item">Price: {{$product['price']}}</li>
                            <li class="list-group-item">Rating: {{$product['rating']}}</li>
                            <li class="list-group-item">Image name: {{$product['img']}}</li>
                        </ul>
                    </div>
                </div>
            @empty
                <div class="col">
                    <p>No products</p>
                </div>
            @endforelse
        </div>
    </div>
</body>
</html>
